# INTRODUCTION

This module integrates with the Drupal log and monolog to move deprecated errors to specific log files,
this way if the dblog is enabled it avoids database writing.



Not necessary if the project has syslog, or other system.

# REQUIREMENTS
* [Monolog](https://www.drupal.org/project/monolog)
* [Monolog extra](https://www.drupal.org/project/monolog_extra)
* Core patch: [Allow parsing PHP constants in YAML files](https://www.drupal.org/project/drupal/issues/2951046#comment-12769907)

# INSTALLATION

This module needs to be installed via Composer, which will also download
the required monolog library and modules. Look at [Using Composer with Drupal](https://www.drupal.org/node/2404989)
for further information.

# CONFIGURATION

## Quick start

You should create a site specific services.yml (Eg: monolog.services.yml) in the
same folder of your settings.php and then add this line to settings.php
itself:

```
$settings['container_yamls'][] = 'sites/default/monolog.services.yml';
```

The simplest configuration that allows Monolog to log to a rotating file the deprecated errors might
be:

```
parameters:
  monolog.channel_handlers:
    default:
      handlers:
        - name: 'drupal.dblog'
    log_deprecated_messages_file_log:
      handlers:
        - name: 'log_deprecated_messages.safe_rotating_handler'
          processors: ['log_deprecated_messages.truncate_messages']
```

It can be enabled or disabled on "/admin/reports/deprecated_logs", also there you can configure the number of
chars for the backtrace on the log.

Look at monolog module documentation for further information.

## How it works

It defines a event subscriber to set a custom handler for deprecation errors, it uses the monolog extra rotating
file system to write on logs.
