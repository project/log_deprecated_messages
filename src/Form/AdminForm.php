<?php

namespace Drupal\log_deprecated_messages\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Log_deprecated_messages admin configuration form.
 */
class AdminForm extends ConfigFormBase {

  /**
   * State.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  private $state;

  /**
   * Construct.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StateInterface $state) {
    parent::__construct($config_factory);
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['log_deprecated_messages.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'log_deprecated_messages_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('log_deprecated_messages.settings');

    $form['log'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Log'),
    ];
    $form['log']['file_logs_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable deprecated messages log'),
      '#default_value' => $config->get('file_logs_enabled'),
      '#description' => $this->t('If enabled the logs related with deprecated messages will be saved on log files.'),
    ];

    $form['log']['trim_backtrace'] = [
      '#type' => 'number',
      '#title' => $this->t('Trim backtrace'),
      '#required' => TRUE,
      '#default_value' => $config->get('trim_backtrace'),
      '#description' => $this->t('Number of backtrace characters to show on the logs.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('log_deprecated_messages.settings');
    $config
      ->set('file_logs_enabled', $form_state->getValue('file_logs_enabled'))
      ->set('trim_backtrace', $form_state->getValue('trim_backtrace'));

    $config->save();
    parent::submitForm($form, $form_state);
  }

}
