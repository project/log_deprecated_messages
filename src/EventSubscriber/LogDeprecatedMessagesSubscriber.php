<?php

namespace Drupal\log_deprecated_messages\EventSubscriber;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\log_deprecated_messages\LogDeprecatedService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Log_deprecated_messages event subscriber.
 */
class LogDeprecatedMessagesSubscriber implements EventSubscriberInterface {

  /**
   * Log deprecated service.
   *
   * @var \Drupal\log_deprecated_messages\LogDeprecatedService
   */
  protected $logDeprecatedService;

  /**
   * Deprecated messages config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $configDeprecatedLogs;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\log_deprecated_messages\LogDeprecatedService $log_deprecated_service
   *   Log deprecated service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   */
  public function __construct(LogDeprecatedService $log_deprecated_service, ConfigFactoryInterface $config_factory) {
    $this->logDeprecatedService = $log_deprecated_service;
    $this->configDeprecatedLogs = $config_factory->get('log_deprecated_messages.settings');
  }

  /**
   * Kernel request event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   Response event.
   */
  public function onKernelRequest(RequestEvent $event) {
    if ($this->configDeprecatedLogs->get('file_logs_enabled')) {
      set_error_handler([$this->logDeprecatedService, 'errorHandler'], E_DEPRECATED);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => ['onKernelRequest'],
    ];
  }

}
