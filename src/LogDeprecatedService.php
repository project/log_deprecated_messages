<?php

namespace Drupal\log_deprecated_messages;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Utility\Error;

/**
 * Log deprecated service.
 */
class LogDeprecatedService {

  const PATH = 'private://log_deprecated_messages/logs/';
  const LOG_PATH = LogDeprecatedService::PATH . 'log_deprecated_messages.log';

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private LoggerChannelInterface $logger;

  /**
   * Log deprecated config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $logDeprecatedConfig;

  /**
   * Construct.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_channel_factory, ConfigFactoryInterface $config_factory) {
    $this->logger = $logger_channel_factory->get('log_deprecated_messages_file_log');
    $this->logDeprecatedConfig = $config_factory->get('log_deprecated_messages.settings');
  }

  /**
   * Custom error handler for deprecated errors.
   *
   * @param int $error_level
   *   The level of the error raised.
   * @param string $message
   *   The error message.
   * @param string $filename
   *   The filename that the error was raised in.
   * @param int $line
   *   The line number the error was raised at.
   *
   * @see _drupal_error_handler_real
   */
  public function errorHandler(int $error_level, string $message, string $filename, int $line) {

    $message_filtered = Xss::filterAdmin($message);
    $backtrace = array_slice(debug_backtrace(), 0, 5);
    $backtrace = array_map(function ($item) {
      if (isset($item['object'])) {
        unset($item['object']);
      }
      return $item;
    }, $backtrace);

    $truncate = $this->logDeprecatedConfig->get('trim_backtrace');
    $backtrace_string = substr(json_encode(array_slice($backtrace, 3)), 0, $truncate) . '...';
    $caller = Error::getLastCaller($backtrace);

    $context = [
      'type' => 'Deprecated function',
      'function' => $caller['function'],
      'file' => $caller['file'],
      'line' => $caller['line'],
      'backtrace' => $backtrace_string,
    ];
    $this->logger->debug($message_filtered, $context);

  }

}
