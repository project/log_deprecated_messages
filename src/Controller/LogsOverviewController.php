<?php

namespace Drupal\log_deprecated_messages\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\log_deprecated_messages\LogDeprecatedService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * List all deprecated log files.
 */
class LogsOverviewController extends ControllerBase {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file_url_generator service.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file_url_generator service.
   */
  public function __construct(FileSystemInterface $file_system, FileUrlGeneratorInterface $file_url_generator) {
    $this->fileSystem = $file_system;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('file_url_generator')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {

    $logs_base_path = LogDeprecatedService::PATH;
    $logs_files = glob($this->fileSystem->realpath($logs_base_path . 'log_deprecated_messages-*.log'));
    $logs_urls = [];
    foreach ($logs_files as $logs_file) {
      $basename = basename($logs_file);
      $logs_urls[] = Link::fromTextAndUrl($basename, Url::fromUri(
        $this->fileUrlGenerator->generateAbsoluteString($logs_base_path . $basename)
      ));
    }

    $build = [];
    if (!empty($logs_urls)) {
      $build['list'] = [
        '#theme' => 'item_list',
        '#items' => $logs_urls,
      ];
    }
    else {
      $build['list'] = [
        '#theme' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('Log files not found.'),
      ];
    }

    return $build;
  }

}
